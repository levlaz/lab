/**
 * Non interface version
 * @param labelledObj
 */
function printLabel(labelledObj: { label: string }) {
    console.log(labelledObj.label);
}

let myObj = {size: 10, label: "Size 10 Object"};
printLabel(myObj);

/**
 * Interface verison
 * 
 * interface describes the requirements of having 
 * the label property that is a string.
 */
interface LabelledValue {
    label: string;
}

function iPrintLabel(labelledObj: LabelledValue) {
    console.log(labelledObj.label);
}

let mySecondObj = {size: 10, label: "Size 10 Object"};
iPrintLabel(mySecondObj);